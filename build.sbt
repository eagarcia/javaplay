name := """play-java"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.11"

libraryDependencies += javaJdbc
libraryDependencies += cache
libraryDependencies += javaWs
libraryDependencies += "org.mongodb" % "mongodb-driver" % "3.4.2"
libraryDependencies += "org.mongodb.morphia" % "morphia" % "1.3.2"
