package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.User;
import services.UserImpl;
import services.UserManagementService;

import javax.inject.Inject;

public class UserController extends Controller
{
    private UserManagementService userManagementService;

    @Inject
    public UserController(UserManagementService service)
    {
        userManagementService = service;
    }

    public Result listUsers()
    {
        return ok(Json.toJson(userManagementService.listUsers()));
    }

    public Result addUser()
    {
        JsonNode json = request().body().asJson();
        System.out.println(json.toString());
        User user = Json.fromJson(json, UserImpl.class);
        return ok(Json.toJson(userManagementService.addUser(user)));
    }

    public Result updateUser(String id)
    {
        JsonNode json = request().body().asJson();
        System.out.println(json.toString());
        User user = Json.fromJson(json, UserImpl.class);
        return ok(Json.toJson(userManagementService.updateUser(id,user)));
    }

    public Result deleteUser(String id)
    {
        if(id==null)
        {
            return ok("Can't delete, missing ID");
        }

        return ok(Json.toJson(userManagementService.deleteUser(id)));
    }
}
