package services;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

//@Entity("users")
public class UserImpl implements User
{

    @Id
    private String id;
    private String name;
    private String email;

    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }
    @Override
    public String getEmail()
    {
        return email;
    }

    @Override
    public void setEmail(String email)
    {
        this.email = email;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }


}
