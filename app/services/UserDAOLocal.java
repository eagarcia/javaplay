package services;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This DOA access a is a simple in-memory storage (Map)
 * It's a singletion to prevent duplicate entries.
 *
 * @author egarcia
 */
@Singleton
public class UserDAOLocal implements UserDAO
{
    private Map<String, User> users = new HashMap<>();
    private int index = 4;

    public UserDAOLocal()
    {
        //Add a few users
        User user = new UserImpl();
        user.setId("1");
        user.setEmail("email-1");
        user.setName("John Smith");
        users.put(user.getId(), user);
        user = new UserImpl();
        user.setId("2");
        user.setEmail("email-2");
        user.setName("Pepe Gonzales");
        users.put(user.getId(), user);
        user = new UserImpl();
        user.setId("3");
        user.setEmail("email-3");
        user.setName("Chris Martin");
        users.put(user.getId(), user);
    }

    @Override
    public List<User> listAllUsers()
    {
        return new ArrayList<>(users.values());
    }

    @Override
    public User getUser(String key)
    {
        if (key != null)
        {
            return users.get(key);
        }
        return null;
    }

    @Override
    public String addUser(User user)
    {
        if (user != null)
        {
            String pk = String.valueOf(index++);
            user.setId(pk);
            users.put(user.getId(), user);
            return user.getId();
        }

        return null;
    }

    @Override
    public boolean updateUser(String id, User user)
    {
        if (user != null)
        {
            User currentUser = users.get(id);
            //Update only if it exist
            if (currentUser != null)
            {
                user.setId(id);
                users.put(id, user);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteUser(String id)
    {
        if (id != null)
        {
            users.remove(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteAll()
    {
        users.clear();
        return true;
    }
}
