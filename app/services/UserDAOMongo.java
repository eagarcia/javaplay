package services;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteResult;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import javax.inject.Singleton;
import java.util.List;

/**
 * Data Access Object to Monogo DB storage.
 *
 * @author egarcia
 */
@Singleton
public class UserDAOMongo implements UserDAO
{
    private MongoClient mongoClient;
    private final MongoDatabase database;
    private MongoCollection<User> userCollection;
    private Morphia morphia = new Morphia();
    private Datastore datastore;

    public UserDAOMongo()
    {
        MongoClientURI connectionString
            = new MongoClientURI("mongodb://admin:e37b51QoRJiVSkf7@cluster0-shard-00-00-ippwl" +
                                 ".mongodb.net:27017,cluster0-shard-00-01-ippwl.mongodb.net:27017,cluster0-shard-00-02-ippwl.mongodb.net:27017/mobilize?replicaSet=Cluster0-shard-0&ssl=true&authSource=admin");

        morphia.map(User.class);
        mongoClient= new MongoClient(connectionString);
        database = mongoClient.getDatabase("moblize");
        userCollection = database.getCollection("user",User.class);
        datastore = morphia.createDatastore(mongoClient, "moblize");
    }

    @Override
    public List<User> listAllUsers()
    {
        return datastore.find(User.class).asList();
    }

    @Override
    public User getUser(String key)
    {
        return datastore.get(UserImpl.class,new ObjectId(key));

    }

    @Override
    public String addUser(User user)
    {
        Key<User> key = datastore.save(user);
        return (String) key.getId();
    }

    @Override
    public boolean updateUser(String id, User user)
    {
        Query<UserImpl> userQuery = datastore.createQuery(UserImpl.class).filter("id = ", new ObjectId(id));
        UpdateOperations<UserImpl> ops = datastore.createUpdateOperations(UserImpl.class)
                                       .set("email",user.getEmail())
                                       .set("name", user.getName());
        datastore.update(userQuery, ops);
        return true;
    }

    @Override
    public boolean deleteUser(String id)
    {
        datastore.delete(UserImpl.class, new ObjectId(id));
        return true;
    }

    @Override
    public boolean deleteAll(){
        MongoCollection<Document> collection = database.getCollection("users");
        collection.drop();
        return true;
    }
}
