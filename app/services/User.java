package services;

import org.mongodb.morphia.annotations.Entity;

@Entity("users")
public interface User
{
    String getId();

    void setId(String id);

    String getEmail();

    void setEmail(String email);

    String getName();

    void setName(String name);
}
