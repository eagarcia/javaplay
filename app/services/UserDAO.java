package services;

import com.google.inject.ImplementedBy;
import org.bson.types.ObjectId;

import java.util.List;

/**
 * There are 2 implementations to this interface: local in-memory map and a MongoDB
 *
 * @author egarcia
 */
@ImplementedBy(UserDAOMongo.class)
public interface UserDAO
{
    List<User> listAllUsers();

    User getUser(String key);

    String addUser(User user);

    boolean updateUser(String id, User user);

    boolean deleteUser(String id);

    boolean deleteAll();
}
