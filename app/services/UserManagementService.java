package services;

import javax.inject.Inject;
import java.util.List;

/**
 * Service to perform CRUD operations with Users
 *
 * @author egarcia
 */
public class UserManagementService
{
    @Inject
    private UserDAO userDAO;

    public List<User> listUsers()
    {
        return userDAO.listAllUsers();
    }

    public User getUser(User user)
    {
        return userDAO.getUser(user.getId());
    }

    public String addUser(User user)
    {
        return userDAO.addUser(user);
    }

    public boolean updateUser(String id, User user)
    {
        return userDAO.updateUser(id,user);
    }

    public boolean deleteUser(String id)
    {
        return userDAO.deleteUser(id);
    }

}
