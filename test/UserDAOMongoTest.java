import org.junit.Before;
import org.junit.Test;
import services.User;
import services.UserDAO;
import services.UserDAOLocal;
import services.UserDAOMongo;
import services.UserImpl;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class UserDAOMongoTest
{
//    private UserDAO userDAO = new UserDAOLocal();
    private UserDAO userDAO = new UserDAOMongo();


    public void cleanDb()
    {
        userDAO.deleteAll();
    }

    @Test
    public void insertUpdateTest(){

        //Insert
        User user = new UserImpl();
        user.setEmail("pedro@yahoo.com");
        user.setName("Pedro Sosa");
        String key = userDAO.addUser(user);
        assertNotNull(key);

        //Get
        user = new UserImpl();
        user.setId(key);
        User userUpdated = userDAO.getUser(user.getId());
        assertNotNull(userUpdated);
        assertEquals("pedro@yahoo.com",userUpdated.getEmail());
        assertEquals("Pedro Sosa",userUpdated.getName());

        //Update
        userUpdated.setEmail("pedro2@gmail.com");
        userDAO.updateUser(userUpdated.getId(),userUpdated);

        //Get
        User user3 = userDAO.getUser(userUpdated.getId());
        assertEquals("pedro2@gmail.com",user3.getEmail());

    }

    @Test
    public void deleteUser()
    {
        User user = new UserImpl();
        user.setEmail("deleteme@aol.com");
        user.setName("Juan Ramos");
        String key = userDAO.addUser(user);

        User user2= userDAO.getUser(user.getId());
        assertNotNull(user2);
        //Delete
        userDAO.deleteUser(key);
        user2= userDAO.getUser(user.getId());
        assertNull(user2);
    }

    @Test
    public void listAllUsersTest()
    {
        List<User> list = userDAO.listAllUsers();
        assertNotNull(list);
        assertTrue(list.size() > 0);

        cleanDb();
    }
}
