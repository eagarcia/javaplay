# README #

Simple Rest application (not UI) that provides CRUD operations for a Users database.

Users consist of only 3 fields

* id
* name
* email


### Rest API ###

####  List user ####

GET http://localhost:9000/users
#### Add User ####

POST http://localhost:9000/users/add

Sample JSON payload:

`{
        "email": "egarcia@somemail.com",
        "name": "Enrique Garcia"
}`

#### Update user ####

http://localhost:9000/users/update/:id

Where **:id** is the key of the user to update
Sample JSON payload (provide "id" in the url):

`{
        "email": "egarcia@yahoo.com",
        "name": "Mr Enrique A Garcia "
}
`
####  Delete user #### 

http://localhost:9000/users/delete/:id

Where **:id** is the key of the user to delete


# Database #

The Mongo DB is hosted in **mongodb.net** cloud service

`cluster0-shard-00-00-ippwl.mongodb.net:27017,cluster0-shard-00-01-ippwl.mongodb.net:27017,cluster0-shard-00-02-ippwl.mongodb.net:27017`
                                 